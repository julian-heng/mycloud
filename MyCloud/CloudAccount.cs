﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MyCloud
{
    public abstract class CloudAccount
    {
        protected const string LOOPBACK_HOST = "http://127.0.0.1:12255/";

        protected string _accessToken = null;

        public bool Initialized { get; set; } = false;
        protected string AccessToken { get => _accessToken; }

        protected readonly Uri RedirectUri = new Uri($"{LOOPBACK_HOST}authorize");
        protected readonly Uri JSRedirectUri = new Uri($"{LOOPBACK_HOST}token");

        public abstract Task Init();
        public abstract Task SetAccessToken();

        public abstract Task<string> GetAccountEmail();
        public abstract Task<List<FileEntry>> GetAccountFiles(string path);
    }

    public abstract class CloudAccountException : Exception
    {
        public CloudAccountException(string msg) : base(msg) { }
        public CloudAccountException(string msg, Exception e) : base(msg, e) { }
    }
}
