﻿using Dropbox.Api;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace MyCloud
{
    class DropboxAccount : CloudAccount
    {
        private const string REDIRECT_HTML = @"
            <html>
            <script type=""text/javascript"">
                function redirect() {
                    document.location.href = ""/token?url_with_fragment="" + encodeURIComponent(document.location.href);
                }
            </script>
            <body onload=""redirect()""/>
            </html>";

        private string DropboxClientId { get => ConfigurationManager.AppSettings["Dropbox_ClientId"]; }

        private DropboxClientConfig _clientConfig = null;
        private DropboxClientConfig ClientConfig { get => _clientConfig; }

        public DropboxAccount() : base()
        {
            _clientConfig = new DropboxClientConfig("MyCloud")
            {
                HttpClient = new HttpClient()
                {
                    Timeout = TimeSpan.FromMinutes(20)
                }
            };
        }

        public override async Task Init()
        {
            DropboxCertHelper.InitializeCertPinning();

            await SetAccessToken();

            if (string.IsNullOrEmpty(AccessToken))
                throw new DropboxAccountException("Failed to set token");
            else
                Initialized = true;
        }

        public override async Task SetAccessToken()
        {
            // Access token is not initialised
            if (string.IsNullOrEmpty(this._accessToken))
            {
                try
                {
                    Debug.WriteLine("Waiting for credentials.");

                    var state = Guid.NewGuid().ToString("N");
                    var authorizeUri = DropboxOAuth2Helper.GetAuthorizeUri(OAuthResponseType.Token,
                                                                           DropboxClientId,
                                                                           RedirectUri,
                                                                           state: state);

                    using (var http = new HttpListener())
                    {
                        http.Prefixes.Add(LOOPBACK_HOST);

                        http.Start();

                        Utils.OpenUrl(authorizeUri.ToString());

                        await HandleOAuth2Redirect(http);
                        var result = await HandleJSRedirect(http);

                        if (result.State != state)
                            throw new DropboxAccountException("State does not match");

                        Debug.WriteLine($"Got access token");
                        this._accessToken = result.AccessToken;
                    }
                }
                catch (Exception e)
                {
                    throw new DropboxAccountException(e.Message, e);
                }
            }
        }

        private async Task HandleOAuth2Redirect(HttpListener http)
        {
            var context = await http.GetContextAsync();

            while (context.Request.Url.AbsolutePath != RedirectUri.AbsolutePath)
                context = await http.GetContextAsync();

            context.Response.ContentType = "text/html";

            using (var stream = new MemoryStream())
            {
                using (var writer = new StreamWriter(stream))
                {
                    writer.Write(REDIRECT_HTML);
                    writer.Flush();
                    stream.Position = 0;

                    stream.CopyTo(context.Response.OutputStream);
                }
            }

            context.Response.OutputStream.Close();
        }

        private async Task<OAuth2Response> HandleJSRedirect(HttpListener http)
        {
            HttpListenerContext context = await http.GetContextAsync();
            while (context.Request.Url.AbsolutePath != JSRedirectUri.AbsolutePath)
                context = await http.GetContextAsync();
            return DropboxOAuth2Helper.ParseTokenFragment(new Uri(context.Request.QueryString["url_with_fragment"]));
        }

        public override async Task<string> GetAccountEmail()
        {
            if (!Initialized)
                throw new DropboxAccountException("Account is not initalised. Please call Init() first.");

            string email = null;

            using (var DbxClient = new DropboxClient(AccessToken, ClientConfig))
            {
                var acc = await DbxClient.Users.GetCurrentAccountAsync();
                email = acc?.Email;
            }

            return email;
        }

        public override async Task<List<FileEntry>> GetAccountFiles(string path)
        {
            List<FileEntry> files = new List<FileEntry>();
            Stack<string> folders = new Stack<string>();

            folders.Push(path == "/" ? "" : path);

            using (var dbx = new DropboxClient(AccessToken, ClientConfig))
            {
                while (folders.Count > 0)
                {
                    var currentPath = folders.Pop();

                    var list = await dbx.Files.ListFolderAsync(currentPath);
                    foreach (var entry in list.Entries)
                    {
                        var actualPath = $"{currentPath}/{entry.Name}";
                        if (entry.IsFolder)
                        {
                            folders.Push(actualPath);
                        }
                        else
                        {
                            files.Add(new FileEntry(actualPath));
                        }
                    }
                }
            }

            return files;
        }
    }

    public class DropboxAccountException : CloudAccountException
    {
        public DropboxAccountException(string msg) : base(msg) { }
        public DropboxAccountException(string msg, Exception e) : base(msg, e) { }
    }
}