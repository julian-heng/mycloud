﻿namespace MyCloud
{
    public class FileEntry
    {
        private string _name;
        public string Name { get => _name; }

        public FileEntry(string _name)
        {
            this._name = _name;
        }
    }
}
