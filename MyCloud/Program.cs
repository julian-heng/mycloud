﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;

namespace MyCloud
{
    class Program
    {
        static void Main(string[] args)
        {
            var dropbox = new DropboxAccount();

            try
            {
                var initTask = Task.Run(dropbox.Init);
                initTask.Wait();
            }
            catch (AggregateException ae)
            {
                foreach (var e in ae.InnerExceptions)
                    Debug.WriteLine(e.Message);
            }

            try
            {
                var emailTask = Task.Run(dropbox.GetAccountEmail);
                emailTask.Wait();
                var email = emailTask.Result;
                Debug.WriteLine(email);
            }
            catch (AggregateException ae)
            {
                foreach (var e in ae.InnerExceptions)
                    Debug.WriteLine(e.Message);
            }

            try
            {
                var filesTask = Task.Run(() => dropbox.GetAccountFiles("/"));
                filesTask.Wait();
                var files = filesTask.Result;
                foreach (var file in files)
                {
                    Debug.WriteLine(file.Name);
                }
            }
            catch (AggregateException ae)
            {
                foreach (var e in ae.InnerExceptions)
                    Debug.WriteLine(e.Message);
            }
        }
    }
}